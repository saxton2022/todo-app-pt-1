import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };
  handleAddTodo = (event) => {
  if (event.key === "Enter") {
    let newTodo = {title: event.target.value, userId: 1, id: Math.random()*10000, completed: false}

    this.setState({todos: [...this.state.todos, newTodo]})

    event.target.value= ""
   }
  }
  handleDelete = todoId => {
    const newTodos = this.state.todos.filter(
      todo => todo.id !== todoId
    )
    this.setState({ todos: newTodos })

  }
  handleToggle = todoId => {
    let newTodos = this.state.todos.map(todo => {
      if(todo.id === todoId) {
        return {
          ...todo,
          completed: !todo.completed
        }
      }
    return {...todo}
    })
    this.setState({ todos: newTodos})

  }
  handleClear = () => {
    const newTodos = this.state.todos.filter(
      todo => todo.completed === false
    )
    this.setState({ todos: newTodos })
  }
  ;


  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" autofocus onKeyPress={this.handleAddTodo}/>

        </header>
        <TodoList todos={this.state.todos} deleteTodo={this.handleDelete} toggleTodo={this.handleToggle}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleClear}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={this.props.completed} onClick={(event)=> this.props.toggleTodo(this.props.id)}/>
          <label>{this.props.title}</label>
          <button className="destroy" onClick={(event)=> this.props.deleteTodo(this.props.id)} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem title={todo.title} completed={todo.completed} deleteTodo={this.props.deleteTodo} id={todo.id} key={todo.id} toggleTodo={this.props.toggleTodo}/>
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
